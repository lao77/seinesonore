package com.mycompany.myapp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Party.
 */
@Entity
@Table(name = "party")
public class Party implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "idfacebook")
    private String idfacebook;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "picture")
    private String picture;

    @Column(name = "ticketuri")
    private String ticketuri;

    @Column(name = "starttime")
    private String starttime;

    @Column(name = "endtime")
    private String endtime;

    @Column(name = "category")
    private String category;

    @Column(name = "festival")
    private Boolean festival;

    @Column(name = "hasafter")
    private Boolean hasafter;

    @Column(name = "popular")
    private Boolean popular;

    @Column(name = "facebooklink")
    private String facebooklink;

    @Column(name = "codepromo")
    private String codepromo;

    @Column(name = "contest")
    private String contest;

    @ManyToOne
    private Place place;

    @ManyToMany
    @JoinTable(name = "party_planner",
               joinColumns = @JoinColumn(name="parties_id", referencedColumnName="id"),
               inverseJoinColumns = @JoinColumn(name="planners_id", referencedColumnName="id"))
    private Set<Planner> planners = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdfacebook() {
        return idfacebook;
    }

    public Party idfacebook(String idfacebook) {
        this.idfacebook = idfacebook;
        return this;
    }

    public void setIdfacebook(String idfacebook) {
        this.idfacebook = idfacebook;
    }

    public String getName() {
        return name;
    }

    public Party name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Party description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public Party picture(String picture) {
        this.picture = picture;
        return this;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTicketuri() {
        return ticketuri;
    }

    public Party ticketuri(String ticketuri) {
        this.ticketuri = ticketuri;
        return this;
    }

    public void setTicketuri(String ticketuri) {
        this.ticketuri = ticketuri;
    }

    public String getStarttime() {
        return starttime;
    }

    public Party starttime(String starttime) {
        this.starttime = starttime;
        return this;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public Party endtime(String endtime) {
        this.endtime = endtime;
        return this;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getCategory() {
        return category;
    }

    public Party category(String category) {
        this.category = category;
        return this;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean isFestival() {
        return festival;
    }

    public Party festival(Boolean festival) {
        this.festival = festival;
        return this;
    }

    public void setFestival(Boolean festival) {
        this.festival = festival;
    }

    public Boolean isHasafter() {
        return hasafter;
    }

    public Party hasafter(Boolean hasafter) {
        this.hasafter = hasafter;
        return this;
    }

    public void setHasafter(Boolean hasafter) {
        this.hasafter = hasafter;
    }

    public Boolean isPopular() {
        return popular;
    }

    public Party popular(Boolean popular) {
        this.popular = popular;
        return this;
    }

    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    public String getFacebooklink() {
        return facebooklink;
    }

    public Party facebooklink(String facebooklink) {
        this.facebooklink = facebooklink;
        return this;
    }

    public void setFacebooklink(String facebooklink) {
        this.facebooklink = facebooklink;
    }

    public String getCodepromo() {
        return codepromo;
    }

    public Party codepromo(String codepromo) {
        this.codepromo = codepromo;
        return this;
    }

    public void setCodepromo(String codepromo) {
        this.codepromo = codepromo;
    }

    public String getContest() {
        return contest;
    }

    public Party contest(String contest) {
        this.contest = contest;
        return this;
    }

    public void setContest(String contest) {
        this.contest = contest;
    }

    public Place getPlace() {
        return place;
    }

    public Party place(Place place) {
        this.place = place;
        return this;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Set<Planner> getPlanners() {
        return planners;
    }

    public Party planners(Set<Planner> planners) {
        this.planners = planners;
        return this;
    }

    public Party addPlanner(Planner planner) {
        this.planners.add(planner);
        return this;
    }

    public Party removePlanner(Planner planner) {
        this.planners.remove(planner);
        return this;
    }

    public void setPlanners(Set<Planner> planners) {
        this.planners = planners;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Party party = (Party) o;
        if (party.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), party.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Party{" +
            "id=" + getId() +
            ", idfacebook='" + getIdfacebook() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", picture='" + getPicture() + "'" +
            ", ticketuri='" + getTicketuri() + "'" +
            ", starttime='" + getStarttime() + "'" +
            ", endtime='" + getEndtime() + "'" +
            ", category='" + getCategory() + "'" +
            ", festival='" + isFestival() + "'" +
            ", hasafter='" + isHasafter() + "'" +
            ", popular='" + isPopular() + "'" +
            ", facebooklink='" + getFacebooklink() + "'" +
            ", codepromo='" + getCodepromo() + "'" +
            ", contest='" + getContest() + "'" +
            "}";
    }
}
