package com.mycompany.myapp.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Planner.
 */
@Entity
@Table(name = "planner")
public class Planner implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "idfacebook")
    private String idfacebook;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "pagefan")
    private String pagefan;

    @Column(name = "picture")
    private String picture;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdfacebook() {
        return idfacebook;
    }

    public Planner idfacebook(String idfacebook) {
        this.idfacebook = idfacebook;
        return this;
    }

    public void setIdfacebook(String idfacebook) {
        this.idfacebook = idfacebook;
    }

    public String getName() {
        return name;
    }

    public Planner name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Planner description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPagefan() {
        return pagefan;
    }

    public Planner pagefan(String pagefan) {
        this.pagefan = pagefan;
        return this;
    }

    public void setPagefan(String pagefan) {
        this.pagefan = pagefan;
    }

    public String getPicture() {
        return picture;
    }

    public Planner picture(String picture) {
        this.picture = picture;
        return this;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Planner planner = (Planner) o;
        if (planner.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), planner.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Planner{" +
            "id=" + getId() +
            ", idfacebook='" + getIdfacebook() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", pagefan='" + getPagefan() + "'" +
            ", picture='" + getPicture() + "'" +
            "}";
    }
}
