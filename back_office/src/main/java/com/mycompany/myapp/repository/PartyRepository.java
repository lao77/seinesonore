package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Party;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Party entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartyRepository extends JpaRepository<Party, Long> {
    @Query("select distinct party from Party party left join fetch party.planners")
    List<Party> findAllWithEagerRelationships();

    @Query("select party from Party party left join fetch party.planners where party.id =:id")
    Party findOneWithEagerRelationships(@Param("id") Long id);

}
