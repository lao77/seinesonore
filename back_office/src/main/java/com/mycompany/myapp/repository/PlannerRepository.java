package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Planner;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Planner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PlannerRepository extends JpaRepository<Planner, Long> {

}
