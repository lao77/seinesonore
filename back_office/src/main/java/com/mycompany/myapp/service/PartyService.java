package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.PartyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Party.
 */
public interface PartyService {

    /**
     * Save a party.
     *
     * @param partyDTO the entity to save
     * @return the persisted entity
     */
    PartyDTO save(PartyDTO partyDTO);

    /**
     * Get all the parties.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PartyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" party.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PartyDTO findOne(Long id);

    /**
     * Delete the "id" party.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
