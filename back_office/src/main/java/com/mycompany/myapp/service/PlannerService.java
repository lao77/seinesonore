package com.mycompany.myapp.service;

import com.mycompany.myapp.service.dto.PlannerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Planner.
 */
public interface PlannerService {

    /**
     * Save a planner.
     *
     * @param plannerDTO the entity to save
     * @return the persisted entity
     */
    PlannerDTO save(PlannerDTO plannerDTO);

    /**
     * Get all the planners.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PlannerDTO> findAll(Pageable pageable);

    /**
     * Get the "id" planner.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PlannerDTO findOne(Long id);

    /**
     * Delete the "id" planner.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
