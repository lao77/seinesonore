package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DTO for the Party entity.
 */
public class PartyDTO implements Serializable {

    private Long id;

    private String idfacebook;

    private String name;

    private String description;

    private String picture;

    private String ticketuri;

    private String starttime;

    private String endtime;

    private String category;

    private Boolean festival;

    private Boolean hasafter;

    private Boolean popular;

    private String facebooklink;

    private String codepromo;

    private String contest;

    private Long placeId;

    private String placeId_facebook;

    private Set<PlannerDTO> planners = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdfacebook() {
        return idfacebook;
    }

    public void setIdfacebook(String idfacebook) {
        this.idfacebook = idfacebook;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getTicketuri() {
        return ticketuri;
    }

    public void setTicketuri(String ticketuri) {
        this.ticketuri = ticketuri;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean isFestival() {
        return festival;
    }

    public void setFestival(Boolean festival) {
        this.festival = festival;
    }

    public Boolean isHasafter() {
        return hasafter;
    }

    public void setHasafter(Boolean hasafter) {
        this.hasafter = hasafter;
    }

    public Boolean isPopular() {
        return popular;
    }

    public void setPopular(Boolean popular) {
        this.popular = popular;
    }

    public String getFacebooklink() {
        return facebooklink;
    }

    public void setFacebooklink(String facebooklink) {
        this.facebooklink = facebooklink;
    }

    public String getCodepromo() {
        return codepromo;
    }

    public void setCodepromo(String codepromo) {
        this.codepromo = codepromo;
    }

    public String getContest() {
        return contest;
    }

    public void setContest(String contest) {
        this.contest = contest;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public String getPlaceId_facebook() {
        return placeId_facebook;
    }

    public void setPlaceId_facebook(String placeId_facebook) {
        this.placeId_facebook = placeId_facebook;
    }

    public Set<PlannerDTO> getPlanners() {
        return planners;
    }

    public void setPlanners(Set<PlannerDTO> planners) {
        this.planners = planners;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PartyDTO partyDTO = (PartyDTO) o;
        if(partyDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), partyDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PartyDTO{" +
            "id=" + getId() +
            ", idfacebook='" + getIdfacebook() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", picture='" + getPicture() + "'" +
            ", ticketuri='" + getTicketuri() + "'" +
            ", starttime='" + getStarttime() + "'" +
            ", endtime='" + getEndtime() + "'" +
            ", category='" + getCategory() + "'" +
            ", festival='" + isFestival() + "'" +
            ", hasafter='" + isHasafter() + "'" +
            ", popular='" + isPopular() + "'" +
            ", facebooklink='" + getFacebooklink() + "'" +
            ", codepromo='" + getCodepromo() + "'" +
            ", contest='" + getContest() + "'" +
            "}";
    }
}
