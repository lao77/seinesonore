package com.mycompany.myapp.service.dto;


import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Planner entity.
 */
public class PlannerDTO implements Serializable {

    private Long id;

    private String idfacebook;

    private String name;

    private String description;

    private String pagefan;

    private String picture;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdfacebook() {
        return idfacebook;
    }

    public void setIdfacebook(String idfacebook) {
        this.idfacebook = idfacebook;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPagefan() {
        return pagefan;
    }

    public void setPagefan(String pagefan) {
        this.pagefan = pagefan;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PlannerDTO plannerDTO = (PlannerDTO) o;
        if(plannerDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), plannerDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlannerDTO{" +
            "id=" + getId() +
            ", idfacebook='" + getIdfacebook() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", pagefan='" + getPagefan() + "'" +
            ", picture='" + getPicture() + "'" +
            "}";
    }
}
