package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.PartyService;
import com.mycompany.myapp.domain.Party;
import com.mycompany.myapp.repository.PartyRepository;
import com.mycompany.myapp.service.dto.PartyDTO;
import com.mycompany.myapp.service.mapper.PartyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Party.
 */
@Service
@Transactional
public class PartyServiceImpl implements PartyService {

    private final Logger log = LoggerFactory.getLogger(PartyServiceImpl.class);

    private final PartyRepository partyRepository;

    private final PartyMapper partyMapper;

    public PartyServiceImpl(PartyRepository partyRepository, PartyMapper partyMapper) {
        this.partyRepository = partyRepository;
        this.partyMapper = partyMapper;
    }

    /**
     * Save a party.
     *
     * @param partyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PartyDTO save(PartyDTO partyDTO) {
        log.debug("Request to save Party : {}", partyDTO);
        Party party = partyMapper.toEntity(partyDTO);
        party = partyRepository.save(party);
        return partyMapper.toDto(party);
    }

    /**
     * Get all the parties.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PartyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Parties");
        return partyRepository.findAll(pageable)
            .map(partyMapper::toDto);
    }

    /**
     * Get one party by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PartyDTO findOne(Long id) {
        log.debug("Request to get Party : {}", id);
        Party party = partyRepository.findOneWithEagerRelationships(id);
        return partyMapper.toDto(party);
    }

    /**
     * Delete the party by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Party : {}", id);
        partyRepository.delete(id);
    }
}
