package com.mycompany.myapp.service.impl;

import com.mycompany.myapp.service.PlannerService;
import com.mycompany.myapp.domain.Planner;
import com.mycompany.myapp.repository.PlannerRepository;
import com.mycompany.myapp.service.dto.PlannerDTO;
import com.mycompany.myapp.service.mapper.PlannerMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing Planner.
 */
@Service
@Transactional
public class PlannerServiceImpl implements PlannerService {

    private final Logger log = LoggerFactory.getLogger(PlannerServiceImpl.class);

    private final PlannerRepository plannerRepository;

    private final PlannerMapper plannerMapper;

    public PlannerServiceImpl(PlannerRepository plannerRepository, PlannerMapper plannerMapper) {
        this.plannerRepository = plannerRepository;
        this.plannerMapper = plannerMapper;
    }

    /**
     * Save a planner.
     *
     * @param plannerDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlannerDTO save(PlannerDTO plannerDTO) {
        log.debug("Request to save Planner : {}", plannerDTO);
        Planner planner = plannerMapper.toEntity(plannerDTO);
        planner = plannerRepository.save(planner);
        return plannerMapper.toDto(planner);
    }

    /**
     * Get all the planners.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PlannerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Planners");
        return plannerRepository.findAll(pageable)
            .map(plannerMapper::toDto);
    }

    /**
     * Get one planner by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlannerDTO findOne(Long id) {
        log.debug("Request to get Planner : {}", id);
        Planner planner = plannerRepository.findOne(id);
        return plannerMapper.toDto(planner);
    }

    /**
     * Delete the planner by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Planner : {}", id);
        plannerRepository.delete(id);
    }
}
