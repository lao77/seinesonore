package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.PartyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Party and its DTO PartyDTO.
 */
@Mapper(componentModel = "spring", uses = {PlaceMapper.class, PlannerMapper.class})
public interface PartyMapper extends EntityMapper<PartyDTO, Party> {

    @Mapping(source = "place.id", target = "placeId")
    @Mapping(source = "place.idfacebook", target = "placeId_facebook")
    PartyDTO toDto(Party party);

    @Mapping(source = "placeId", target = "place")
    Party toEntity(PartyDTO partyDTO);

    default Party fromId(Long id) {
        if (id == null) {
            return null;
        }
        Party party = new Party();
        party.setId(id);
        return party;
    }
}
