package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.*;
import com.mycompany.myapp.service.dto.PlannerDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Planner and its DTO PlannerDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PlannerMapper extends EntityMapper<PlannerDTO, Planner> {



    default Planner fromId(Long id) {
        if (id == null) {
            return null;
        }
        Planner planner = new Planner();
        planner.setId(id);
        return planner;
    }
}
