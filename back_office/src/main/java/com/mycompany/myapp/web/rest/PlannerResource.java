package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.service.PlannerService;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import com.mycompany.myapp.service.dto.PlannerDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Planner.
 */
@RestController
@RequestMapping("/api")
public class PlannerResource {

    private final Logger log = LoggerFactory.getLogger(PlannerResource.class);

    private static final String ENTITY_NAME = "planner";

    private final PlannerService plannerService;

    public PlannerResource(PlannerService plannerService) {
        this.plannerService = plannerService;
    }

    /**
     * POST  /planners : Create a new planner.
     *
     * @param plannerDTO the plannerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new plannerDTO, or with status 400 (Bad Request) if the planner has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/planners")
    @Timed
    public ResponseEntity<PlannerDTO> createPlanner(@RequestBody PlannerDTO plannerDTO) throws URISyntaxException {
        log.debug("REST request to save Planner : {}", plannerDTO);
        if (plannerDTO.getId() != null) {
            throw new BadRequestAlertException("A new planner cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PlannerDTO result = plannerService.save(plannerDTO);
        return ResponseEntity.created(new URI("/api/planners/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /planners : Updates an existing planner.
     *
     * @param plannerDTO the plannerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated plannerDTO,
     * or with status 400 (Bad Request) if the plannerDTO is not valid,
     * or with status 500 (Internal Server Error) if the plannerDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/planners")
    @Timed
    public ResponseEntity<PlannerDTO> updatePlanner(@RequestBody PlannerDTO plannerDTO) throws URISyntaxException {
        log.debug("REST request to update Planner : {}", plannerDTO);
        if (plannerDTO.getId() == null) {
            return createPlanner(plannerDTO);
        }
        PlannerDTO result = plannerService.save(plannerDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, plannerDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /planners : get all the planners.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of planners in body
     */
    @GetMapping("/planners")
    @Timed
    public ResponseEntity<List<PlannerDTO>> getAllPlanners(Pageable pageable) {
        log.debug("REST request to get a page of Planners");
        Page<PlannerDTO> page = plannerService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/planners");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /planners/:id : get the "id" planner.
     *
     * @param id the id of the plannerDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the plannerDTO, or with status 404 (Not Found)
     */
    @GetMapping("/planners/{id}")
    @Timed
    public ResponseEntity<PlannerDTO> getPlanner(@PathVariable Long id) {
        log.debug("REST request to get Planner : {}", id);
        PlannerDTO plannerDTO = plannerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(plannerDTO));
    }

    /**
     * DELETE  /planners/:id : delete the "id" planner.
     *
     * @param id the id of the plannerDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/planners/{id}")
    @Timed
    public ResponseEntity<Void> deletePlanner(@PathVariable Long id) {
        log.debug("REST request to delete Planner : {}", id);
        plannerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
