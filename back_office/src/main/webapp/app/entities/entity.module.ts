import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BackOfficePartyModule } from './party/party.module';
import { BackOfficePlaceModule } from './place/place.module';
import { BackOfficePlannerModule } from './planner/planner.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        BackOfficePartyModule,
        BackOfficePlaceModule,
        BackOfficePlannerModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackOfficeEntityModule {}
