import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Party } from './party.model';
import { PartyPopupService } from './party-popup.service';
import { PartyService } from './party.service';
import { Place, PlaceService } from '../place';
import { Planner, PlannerService } from '../planner';

@Component({
    selector: 'jhi-party-dialog',
    templateUrl: './party-dialog.component.html'
})
export class PartyDialogComponent implements OnInit {

    party: Party;
    isSaving: boolean;

    places: Place[];

    planners: Planner[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private partyService: PartyService,
        private placeService: PlaceService,
        private plannerService: PlannerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.placeService.query()
            .subscribe((res: HttpResponse<Place[]>) => { this.places = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.plannerService.query()
            .subscribe((res: HttpResponse<Planner[]>) => { this.planners = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.party.id !== undefined) {
            this.subscribeToSaveResponse(
                this.partyService.update(this.party));
        } else {
            this.subscribeToSaveResponse(
                this.partyService.create(this.party));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Party>>) {
        result.subscribe((res: HttpResponse<Party>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Party) {
        this.eventManager.broadcast({ name: 'partyListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackPlaceById(index: number, item: Place) {
        return item.id;
    }

    trackPlannerById(index: number, item: Planner) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-party-popup',
    template: ''
})
export class PartyPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private partyPopupService: PartyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.partyPopupService
                    .open(PartyDialogComponent as Component, params['id']);
            } else {
                this.partyPopupService
                    .open(PartyDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
