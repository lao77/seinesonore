import { BaseEntity } from './../../shared';

export class Party implements BaseEntity {
    constructor(
        public id?: number,
        public idfacebook?: string,
        public name?: string,
        public description?: string,
        public picture?: string,
        public ticketuri?: string,
        public starttime?: string,
        public endtime?: string,
        public category?: string,
        public festival?: boolean,
        public hasafter?: boolean,
        public popular?: boolean,
        public facebooklink?: string,
        public codepromo?: string,
        public contest?: string,
        public placeId_facebook?: string,
        public placeId?: number,
        public planners?: BaseEntity[],
    ) {
        this.festival = false;
        this.hasafter = false;
        this.popular = false;
    }
}
