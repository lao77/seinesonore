import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackOfficeSharedModule } from '../../shared';
import {
    PartyService,
    PartyPopupService,
    PartyComponent,
    PartyDetailComponent,
    PartyDialogComponent,
    PartyPopupComponent,
    PartyDeletePopupComponent,
    PartyDeleteDialogComponent,
    partyRoute,
    partyPopupRoute,
    PartyResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...partyRoute,
    ...partyPopupRoute,
];

@NgModule({
    imports: [
        BackOfficeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PartyComponent,
        PartyDetailComponent,
        PartyDialogComponent,
        PartyDeleteDialogComponent,
        PartyPopupComponent,
        PartyDeletePopupComponent,
    ],
    entryComponents: [
        PartyComponent,
        PartyDialogComponent,
        PartyPopupComponent,
        PartyDeleteDialogComponent,
        PartyDeletePopupComponent,
    ],
    providers: [
        PartyService,
        PartyPopupService,
        PartyResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackOfficePartyModule {}
