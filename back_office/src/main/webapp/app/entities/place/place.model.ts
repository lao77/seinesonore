import { BaseEntity } from './../../shared';

export class Place implements BaseEntity {
    constructor(
        public id?: number,
        public idfacebook?: string,
        public name?: string,
        public street?: string,
        public zip?: string,
        public city?: string,
        public country?: string,
        public longitude?: string,
        public latitude?: string,
        public picture?: string,
        public locatedin?: string,
    ) {
    }
}
