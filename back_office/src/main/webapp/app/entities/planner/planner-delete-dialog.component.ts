import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Planner } from './planner.model';
import { PlannerPopupService } from './planner-popup.service';
import { PlannerService } from './planner.service';

@Component({
    selector: 'jhi-planner-delete-dialog',
    templateUrl: './planner-delete-dialog.component.html'
})
export class PlannerDeleteDialogComponent {

    planner: Planner;

    constructor(
        private plannerService: PlannerService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.plannerService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'plannerListModification',
                content: 'Deleted an planner'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-planner-delete-popup',
    template: ''
})
export class PlannerDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private plannerPopupService: PlannerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.plannerPopupService
                .open(PlannerDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
