import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Planner } from './planner.model';
import { PlannerService } from './planner.service';

@Component({
    selector: 'jhi-planner-detail',
    templateUrl: './planner-detail.component.html'
})
export class PlannerDetailComponent implements OnInit, OnDestroy {

    planner: Planner;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private plannerService: PlannerService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPlanners();
    }

    load(id) {
        this.plannerService.find(id)
            .subscribe((plannerResponse: HttpResponse<Planner>) => {
                this.planner = plannerResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPlanners() {
        this.eventSubscriber = this.eventManager.subscribe(
            'plannerListModification',
            (response) => this.load(this.planner.id)
        );
    }
}
