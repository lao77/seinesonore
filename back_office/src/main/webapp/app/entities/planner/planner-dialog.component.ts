import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Planner } from './planner.model';
import { PlannerPopupService } from './planner-popup.service';
import { PlannerService } from './planner.service';

@Component({
    selector: 'jhi-planner-dialog',
    templateUrl: './planner-dialog.component.html'
})
export class PlannerDialogComponent implements OnInit {

    planner: Planner;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private plannerService: PlannerService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.planner.id !== undefined) {
            this.subscribeToSaveResponse(
                this.plannerService.update(this.planner));
        } else {
            this.subscribeToSaveResponse(
                this.plannerService.create(this.planner));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Planner>>) {
        result.subscribe((res: HttpResponse<Planner>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Planner) {
        this.eventManager.broadcast({ name: 'plannerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-planner-popup',
    template: ''
})
export class PlannerPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private plannerPopupService: PlannerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.plannerPopupService
                    .open(PlannerDialogComponent as Component, params['id']);
            } else {
                this.plannerPopupService
                    .open(PlannerDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
