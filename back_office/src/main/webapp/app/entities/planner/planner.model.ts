import { BaseEntity } from './../../shared';

export class Planner implements BaseEntity {
    constructor(
        public id?: number,
        public idfacebook?: string,
        public name?: string,
        public description?: string,
        public pagefan?: string,
        public picture?: string,
    ) {
    }
}
