import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BackOfficeSharedModule } from '../../shared';
import {
    PlannerService,
    PlannerPopupService,
    PlannerComponent,
    PlannerDetailComponent,
    PlannerDialogComponent,
    PlannerPopupComponent,
    PlannerDeletePopupComponent,
    PlannerDeleteDialogComponent,
    plannerRoute,
    plannerPopupRoute,
    PlannerResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...plannerRoute,
    ...plannerPopupRoute,
];

@NgModule({
    imports: [
        BackOfficeSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PlannerComponent,
        PlannerDetailComponent,
        PlannerDialogComponent,
        PlannerDeleteDialogComponent,
        PlannerPopupComponent,
        PlannerDeletePopupComponent,
    ],
    entryComponents: [
        PlannerComponent,
        PlannerDialogComponent,
        PlannerPopupComponent,
        PlannerDeleteDialogComponent,
        PlannerDeletePopupComponent,
    ],
    providers: [
        PlannerService,
        PlannerPopupService,
        PlannerResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackOfficePlannerModule {}
