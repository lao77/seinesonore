import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PlannerComponent } from './planner.component';
import { PlannerDetailComponent } from './planner-detail.component';
import { PlannerPopupComponent } from './planner-dialog.component';
import { PlannerDeletePopupComponent } from './planner-delete-dialog.component';

@Injectable()
export class PlannerResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const plannerRoute: Routes = [
    {
        path: 'planner',
        component: PlannerComponent,
        resolve: {
            'pagingParams': PlannerResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backOfficeApp.planner.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'planner/:id',
        component: PlannerDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backOfficeApp.planner.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const plannerPopupRoute: Routes = [
    {
        path: 'planner-new',
        component: PlannerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backOfficeApp.planner.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'planner/:id/edit',
        component: PlannerPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backOfficeApp.planner.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'planner/:id/delete',
        component: PlannerDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'backOfficeApp.planner.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
