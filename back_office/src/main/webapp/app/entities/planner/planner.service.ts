import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Planner } from './planner.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Planner>;

@Injectable()
export class PlannerService {

    private resourceUrl =  SERVER_API_URL + 'api/planners';

    constructor(private http: HttpClient) { }

    create(planner: Planner): Observable<EntityResponseType> {
        const copy = this.convert(planner);
        return this.http.post<Planner>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(planner: Planner): Observable<EntityResponseType> {
        const copy = this.convert(planner);
        return this.http.put<Planner>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Planner>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Planner[]>> {
        const options = createRequestOption(req);
        return this.http.get<Planner[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Planner[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Planner = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Planner[]>): HttpResponse<Planner[]> {
        const jsonResponse: Planner[] = res.body;
        const body: Planner[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Planner.
     */
    private convertItemFromServer(planner: Planner): Planner {
        const copy: Planner = Object.assign({}, planner);
        return copy;
    }

    /**
     * Convert a Planner to a JSON which can be sent to the server.
     */
    private convert(planner: Planner): Planner {
        const copy: Planner = Object.assign({}, planner);
        return copy;
    }
}
