package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.BackOfficeApp;

import com.mycompany.myapp.domain.Party;
import com.mycompany.myapp.repository.PartyRepository;
import com.mycompany.myapp.service.PartyService;
import com.mycompany.myapp.service.dto.PartyDTO;
import com.mycompany.myapp.service.mapper.PartyMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PartyResource REST controller.
 *
 * @see PartyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackOfficeApp.class)
public class PartyResourceIntTest {

    private static final String DEFAULT_IDFACEBOOK = "AAAAAAAAAA";
    private static final String UPDATED_IDFACEBOOK = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_PICTURE = "AAAAAAAAAA";
    private static final String UPDATED_PICTURE = "BBBBBBBBBB";

    private static final String DEFAULT_TICKETURI = "AAAAAAAAAA";
    private static final String UPDATED_TICKETURI = "BBBBBBBBBB";

    private static final String DEFAULT_STARTTIME = "AAAAAAAAAA";
    private static final String UPDATED_STARTTIME = "BBBBBBBBBB";

    private static final String DEFAULT_ENDTIME = "AAAAAAAAAA";
    private static final String UPDATED_ENDTIME = "BBBBBBBBBB";

    private static final String DEFAULT_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_CATEGORY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FESTIVAL = false;
    private static final Boolean UPDATED_FESTIVAL = true;

    private static final Boolean DEFAULT_HASAFTER = false;
    private static final Boolean UPDATED_HASAFTER = true;

    private static final Boolean DEFAULT_POPULAR = false;
    private static final Boolean UPDATED_POPULAR = true;

    private static final String DEFAULT_FACEBOOKLINK = "AAAAAAAAAA";
    private static final String UPDATED_FACEBOOKLINK = "BBBBBBBBBB";

    private static final String DEFAULT_CODEPROMO = "AAAAAAAAAA";
    private static final String UPDATED_CODEPROMO = "BBBBBBBBBB";

    private static final String DEFAULT_CONTEST = "AAAAAAAAAA";
    private static final String UPDATED_CONTEST = "BBBBBBBBBB";

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private PartyMapper partyMapper;

    @Autowired
    private PartyService partyService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPartyMockMvc;

    private Party party;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PartyResource partyResource = new PartyResource(partyService);
        this.restPartyMockMvc = MockMvcBuilders.standaloneSetup(partyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Party createEntity(EntityManager em) {
        Party party = new Party()
            .idfacebook(DEFAULT_IDFACEBOOK)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .picture(DEFAULT_PICTURE)
            .ticketuri(DEFAULT_TICKETURI)
            .starttime(DEFAULT_STARTTIME)
            .endtime(DEFAULT_ENDTIME)
            .category(DEFAULT_CATEGORY)
            .festival(DEFAULT_FESTIVAL)
            .hasafter(DEFAULT_HASAFTER)
            .popular(DEFAULT_POPULAR)
            .facebooklink(DEFAULT_FACEBOOKLINK)
            .codepromo(DEFAULT_CODEPROMO)
            .contest(DEFAULT_CONTEST);
        return party;
    }

    @Before
    public void initTest() {
        party = createEntity(em);
    }

    @Test
    @Transactional
    public void createParty() throws Exception {
        int databaseSizeBeforeCreate = partyRepository.findAll().size();

        // Create the Party
        PartyDTO partyDTO = partyMapper.toDto(party);
        restPartyMockMvc.perform(post("/api/parties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partyDTO)))
            .andExpect(status().isCreated());

        // Validate the Party in the database
        List<Party> partyList = partyRepository.findAll();
        assertThat(partyList).hasSize(databaseSizeBeforeCreate + 1);
        Party testParty = partyList.get(partyList.size() - 1);
        assertThat(testParty.getIdfacebook()).isEqualTo(DEFAULT_IDFACEBOOK);
        assertThat(testParty.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testParty.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testParty.getPicture()).isEqualTo(DEFAULT_PICTURE);
        assertThat(testParty.getTicketuri()).isEqualTo(DEFAULT_TICKETURI);
        assertThat(testParty.getStarttime()).isEqualTo(DEFAULT_STARTTIME);
        assertThat(testParty.getEndtime()).isEqualTo(DEFAULT_ENDTIME);
        assertThat(testParty.getCategory()).isEqualTo(DEFAULT_CATEGORY);
        assertThat(testParty.isFestival()).isEqualTo(DEFAULT_FESTIVAL);
        assertThat(testParty.isHasafter()).isEqualTo(DEFAULT_HASAFTER);
        assertThat(testParty.isPopular()).isEqualTo(DEFAULT_POPULAR);
        assertThat(testParty.getFacebooklink()).isEqualTo(DEFAULT_FACEBOOKLINK);
        assertThat(testParty.getCodepromo()).isEqualTo(DEFAULT_CODEPROMO);
        assertThat(testParty.getContest()).isEqualTo(DEFAULT_CONTEST);
    }

    @Test
    @Transactional
    public void createPartyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = partyRepository.findAll().size();

        // Create the Party with an existing ID
        party.setId(1L);
        PartyDTO partyDTO = partyMapper.toDto(party);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPartyMockMvc.perform(post("/api/parties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Party in the database
        List<Party> partyList = partyRepository.findAll();
        assertThat(partyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllParties() throws Exception {
        // Initialize the database
        partyRepository.saveAndFlush(party);

        // Get all the partyList
        restPartyMockMvc.perform(get("/api/parties?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(party.getId().intValue())))
            .andExpect(jsonPath("$.[*].idfacebook").value(hasItem(DEFAULT_IDFACEBOOK.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(DEFAULT_PICTURE.toString())))
            .andExpect(jsonPath("$.[*].ticketuri").value(hasItem(DEFAULT_TICKETURI.toString())))
            .andExpect(jsonPath("$.[*].starttime").value(hasItem(DEFAULT_STARTTIME.toString())))
            .andExpect(jsonPath("$.[*].endtime").value(hasItem(DEFAULT_ENDTIME.toString())))
            .andExpect(jsonPath("$.[*].category").value(hasItem(DEFAULT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].festival").value(hasItem(DEFAULT_FESTIVAL.booleanValue())))
            .andExpect(jsonPath("$.[*].hasafter").value(hasItem(DEFAULT_HASAFTER.booleanValue())))
            .andExpect(jsonPath("$.[*].popular").value(hasItem(DEFAULT_POPULAR.booleanValue())))
            .andExpect(jsonPath("$.[*].facebooklink").value(hasItem(DEFAULT_FACEBOOKLINK.toString())))
            .andExpect(jsonPath("$.[*].codepromo").value(hasItem(DEFAULT_CODEPROMO.toString())))
            .andExpect(jsonPath("$.[*].contest").value(hasItem(DEFAULT_CONTEST.toString())));
    }

    @Test
    @Transactional
    public void getParty() throws Exception {
        // Initialize the database
        partyRepository.saveAndFlush(party);

        // Get the party
        restPartyMockMvc.perform(get("/api/parties/{id}", party.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(party.getId().intValue()))
            .andExpect(jsonPath("$.idfacebook").value(DEFAULT_IDFACEBOOK.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.picture").value(DEFAULT_PICTURE.toString()))
            .andExpect(jsonPath("$.ticketuri").value(DEFAULT_TICKETURI.toString()))
            .andExpect(jsonPath("$.starttime").value(DEFAULT_STARTTIME.toString()))
            .andExpect(jsonPath("$.endtime").value(DEFAULT_ENDTIME.toString()))
            .andExpect(jsonPath("$.category").value(DEFAULT_CATEGORY.toString()))
            .andExpect(jsonPath("$.festival").value(DEFAULT_FESTIVAL.booleanValue()))
            .andExpect(jsonPath("$.hasafter").value(DEFAULT_HASAFTER.booleanValue()))
            .andExpect(jsonPath("$.popular").value(DEFAULT_POPULAR.booleanValue()))
            .andExpect(jsonPath("$.facebooklink").value(DEFAULT_FACEBOOKLINK.toString()))
            .andExpect(jsonPath("$.codepromo").value(DEFAULT_CODEPROMO.toString()))
            .andExpect(jsonPath("$.contest").value(DEFAULT_CONTEST.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingParty() throws Exception {
        // Get the party
        restPartyMockMvc.perform(get("/api/parties/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateParty() throws Exception {
        // Initialize the database
        partyRepository.saveAndFlush(party);
        int databaseSizeBeforeUpdate = partyRepository.findAll().size();

        // Update the party
        Party updatedParty = partyRepository.findOne(party.getId());
        // Disconnect from session so that the updates on updatedParty are not directly saved in db
        em.detach(updatedParty);
        updatedParty
            .idfacebook(UPDATED_IDFACEBOOK)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .picture(UPDATED_PICTURE)
            .ticketuri(UPDATED_TICKETURI)
            .starttime(UPDATED_STARTTIME)
            .endtime(UPDATED_ENDTIME)
            .category(UPDATED_CATEGORY)
            .festival(UPDATED_FESTIVAL)
            .hasafter(UPDATED_HASAFTER)
            .popular(UPDATED_POPULAR)
            .facebooklink(UPDATED_FACEBOOKLINK)
            .codepromo(UPDATED_CODEPROMO)
            .contest(UPDATED_CONTEST);
        PartyDTO partyDTO = partyMapper.toDto(updatedParty);

        restPartyMockMvc.perform(put("/api/parties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partyDTO)))
            .andExpect(status().isOk());

        // Validate the Party in the database
        List<Party> partyList = partyRepository.findAll();
        assertThat(partyList).hasSize(databaseSizeBeforeUpdate);
        Party testParty = partyList.get(partyList.size() - 1);
        assertThat(testParty.getIdfacebook()).isEqualTo(UPDATED_IDFACEBOOK);
        assertThat(testParty.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testParty.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testParty.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testParty.getTicketuri()).isEqualTo(UPDATED_TICKETURI);
        assertThat(testParty.getStarttime()).isEqualTo(UPDATED_STARTTIME);
        assertThat(testParty.getEndtime()).isEqualTo(UPDATED_ENDTIME);
        assertThat(testParty.getCategory()).isEqualTo(UPDATED_CATEGORY);
        assertThat(testParty.isFestival()).isEqualTo(UPDATED_FESTIVAL);
        assertThat(testParty.isHasafter()).isEqualTo(UPDATED_HASAFTER);
        assertThat(testParty.isPopular()).isEqualTo(UPDATED_POPULAR);
        assertThat(testParty.getFacebooklink()).isEqualTo(UPDATED_FACEBOOKLINK);
        assertThat(testParty.getCodepromo()).isEqualTo(UPDATED_CODEPROMO);
        assertThat(testParty.getContest()).isEqualTo(UPDATED_CONTEST);
    }

    @Test
    @Transactional
    public void updateNonExistingParty() throws Exception {
        int databaseSizeBeforeUpdate = partyRepository.findAll().size();

        // Create the Party
        PartyDTO partyDTO = partyMapper.toDto(party);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPartyMockMvc.perform(put("/api/parties")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(partyDTO)))
            .andExpect(status().isCreated());

        // Validate the Party in the database
        List<Party> partyList = partyRepository.findAll();
        assertThat(partyList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteParty() throws Exception {
        // Initialize the database
        partyRepository.saveAndFlush(party);
        int databaseSizeBeforeDelete = partyRepository.findAll().size();

        // Get the party
        restPartyMockMvc.perform(delete("/api/parties/{id}", party.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Party> partyList = partyRepository.findAll();
        assertThat(partyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Party.class);
        Party party1 = new Party();
        party1.setId(1L);
        Party party2 = new Party();
        party2.setId(party1.getId());
        assertThat(party1).isEqualTo(party2);
        party2.setId(2L);
        assertThat(party1).isNotEqualTo(party2);
        party1.setId(null);
        assertThat(party1).isNotEqualTo(party2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PartyDTO.class);
        PartyDTO partyDTO1 = new PartyDTO();
        partyDTO1.setId(1L);
        PartyDTO partyDTO2 = new PartyDTO();
        assertThat(partyDTO1).isNotEqualTo(partyDTO2);
        partyDTO2.setId(partyDTO1.getId());
        assertThat(partyDTO1).isEqualTo(partyDTO2);
        partyDTO2.setId(2L);
        assertThat(partyDTO1).isNotEqualTo(partyDTO2);
        partyDTO1.setId(null);
        assertThat(partyDTO1).isNotEqualTo(partyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(partyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(partyMapper.fromId(null)).isNull();
    }
}
