package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.BackOfficeApp;

import com.mycompany.myapp.domain.Planner;
import com.mycompany.myapp.repository.PlannerRepository;
import com.mycompany.myapp.service.PlannerService;
import com.mycompany.myapp.service.dto.PlannerDTO;
import com.mycompany.myapp.service.mapper.PlannerMapper;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlannerResource REST controller.
 *
 * @see PlannerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackOfficeApp.class)
public class PlannerResourceIntTest {

    private static final String DEFAULT_IDFACEBOOK = "AAAAAAAAAA";
    private static final String UPDATED_IDFACEBOOK = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_PAGEFAN = "AAAAAAAAAA";
    private static final String UPDATED_PAGEFAN = "BBBBBBBBBB";

    private static final String DEFAULT_PICTURE = "AAAAAAAAAA";
    private static final String UPDATED_PICTURE = "BBBBBBBBBB";

    @Autowired
    private PlannerRepository plannerRepository;

    @Autowired
    private PlannerMapper plannerMapper;

    @Autowired
    private PlannerService plannerService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlannerMockMvc;

    private Planner planner;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlannerResource plannerResource = new PlannerResource(plannerService);
        this.restPlannerMockMvc = MockMvcBuilders.standaloneSetup(plannerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Planner createEntity(EntityManager em) {
        Planner planner = new Planner()
            .idfacebook(DEFAULT_IDFACEBOOK)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .pagefan(DEFAULT_PAGEFAN)
            .picture(DEFAULT_PICTURE);
        return planner;
    }

    @Before
    public void initTest() {
        planner = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlanner() throws Exception {
        int databaseSizeBeforeCreate = plannerRepository.findAll().size();

        // Create the Planner
        PlannerDTO plannerDTO = plannerMapper.toDto(planner);
        restPlannerMockMvc.perform(post("/api/planners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannerDTO)))
            .andExpect(status().isCreated());

        // Validate the Planner in the database
        List<Planner> plannerList = plannerRepository.findAll();
        assertThat(plannerList).hasSize(databaseSizeBeforeCreate + 1);
        Planner testPlanner = plannerList.get(plannerList.size() - 1);
        assertThat(testPlanner.getIdfacebook()).isEqualTo(DEFAULT_IDFACEBOOK);
        assertThat(testPlanner.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPlanner.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPlanner.getPagefan()).isEqualTo(DEFAULT_PAGEFAN);
        assertThat(testPlanner.getPicture()).isEqualTo(DEFAULT_PICTURE);
    }

    @Test
    @Transactional
    public void createPlannerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = plannerRepository.findAll().size();

        // Create the Planner with an existing ID
        planner.setId(1L);
        PlannerDTO plannerDTO = plannerMapper.toDto(planner);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlannerMockMvc.perform(post("/api/planners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Planner in the database
        List<Planner> plannerList = plannerRepository.findAll();
        assertThat(plannerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPlanners() throws Exception {
        // Initialize the database
        plannerRepository.saveAndFlush(planner);

        // Get all the plannerList
        restPlannerMockMvc.perform(get("/api/planners?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(planner.getId().intValue())))
            .andExpect(jsonPath("$.[*].idfacebook").value(hasItem(DEFAULT_IDFACEBOOK.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].pagefan").value(hasItem(DEFAULT_PAGEFAN.toString())))
            .andExpect(jsonPath("$.[*].picture").value(hasItem(DEFAULT_PICTURE.toString())));
    }

    @Test
    @Transactional
    public void getPlanner() throws Exception {
        // Initialize the database
        plannerRepository.saveAndFlush(planner);

        // Get the planner
        restPlannerMockMvc.perform(get("/api/planners/{id}", planner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(planner.getId().intValue()))
            .andExpect(jsonPath("$.idfacebook").value(DEFAULT_IDFACEBOOK.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.pagefan").value(DEFAULT_PAGEFAN.toString()))
            .andExpect(jsonPath("$.picture").value(DEFAULT_PICTURE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPlanner() throws Exception {
        // Get the planner
        restPlannerMockMvc.perform(get("/api/planners/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlanner() throws Exception {
        // Initialize the database
        plannerRepository.saveAndFlush(planner);
        int databaseSizeBeforeUpdate = plannerRepository.findAll().size();

        // Update the planner
        Planner updatedPlanner = plannerRepository.findOne(planner.getId());
        // Disconnect from session so that the updates on updatedPlanner are not directly saved in db
        em.detach(updatedPlanner);
        updatedPlanner
            .idfacebook(UPDATED_IDFACEBOOK)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .pagefan(UPDATED_PAGEFAN)
            .picture(UPDATED_PICTURE);
        PlannerDTO plannerDTO = plannerMapper.toDto(updatedPlanner);

        restPlannerMockMvc.perform(put("/api/planners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannerDTO)))
            .andExpect(status().isOk());

        // Validate the Planner in the database
        List<Planner> plannerList = plannerRepository.findAll();
        assertThat(plannerList).hasSize(databaseSizeBeforeUpdate);
        Planner testPlanner = plannerList.get(plannerList.size() - 1);
        assertThat(testPlanner.getIdfacebook()).isEqualTo(UPDATED_IDFACEBOOK);
        assertThat(testPlanner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPlanner.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPlanner.getPagefan()).isEqualTo(UPDATED_PAGEFAN);
        assertThat(testPlanner.getPicture()).isEqualTo(UPDATED_PICTURE);
    }

    @Test
    @Transactional
    public void updateNonExistingPlanner() throws Exception {
        int databaseSizeBeforeUpdate = plannerRepository.findAll().size();

        // Create the Planner
        PlannerDTO plannerDTO = plannerMapper.toDto(planner);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlannerMockMvc.perform(put("/api/planners")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(plannerDTO)))
            .andExpect(status().isCreated());

        // Validate the Planner in the database
        List<Planner> plannerList = plannerRepository.findAll();
        assertThat(plannerList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlanner() throws Exception {
        // Initialize the database
        plannerRepository.saveAndFlush(planner);
        int databaseSizeBeforeDelete = plannerRepository.findAll().size();

        // Get the planner
        restPlannerMockMvc.perform(delete("/api/planners/{id}", planner.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Planner> plannerList = plannerRepository.findAll();
        assertThat(plannerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Planner.class);
        Planner planner1 = new Planner();
        planner1.setId(1L);
        Planner planner2 = new Planner();
        planner2.setId(planner1.getId());
        assertThat(planner1).isEqualTo(planner2);
        planner2.setId(2L);
        assertThat(planner1).isNotEqualTo(planner2);
        planner1.setId(null);
        assertThat(planner1).isNotEqualTo(planner2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlannerDTO.class);
        PlannerDTO plannerDTO1 = new PlannerDTO();
        plannerDTO1.setId(1L);
        PlannerDTO plannerDTO2 = new PlannerDTO();
        assertThat(plannerDTO1).isNotEqualTo(plannerDTO2);
        plannerDTO2.setId(plannerDTO1.getId());
        assertThat(plannerDTO1).isEqualTo(plannerDTO2);
        plannerDTO2.setId(2L);
        assertThat(plannerDTO1).isNotEqualTo(plannerDTO2);
        plannerDTO1.setId(null);
        assertThat(plannerDTO1).isNotEqualTo(plannerDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(plannerMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(plannerMapper.fromId(null)).isNull();
    }
}
