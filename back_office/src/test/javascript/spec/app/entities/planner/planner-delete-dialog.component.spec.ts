/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { BackOfficeTestModule } from '../../../test.module';
import { PlannerDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/planner/planner-delete-dialog.component';
import { PlannerService } from '../../../../../../main/webapp/app/entities/planner/planner.service';

describe('Component Tests', () => {

    describe('Planner Management Delete Component', () => {
        let comp: PlannerDeleteDialogComponent;
        let fixture: ComponentFixture<PlannerDeleteDialogComponent>;
        let service: PlannerService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackOfficeTestModule],
                declarations: [PlannerDeleteDialogComponent],
                providers: [
                    PlannerService
                ]
            })
            .overrideTemplate(PlannerDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PlannerDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlannerService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
