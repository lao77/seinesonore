/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BackOfficeTestModule } from '../../../test.module';
import { PlannerComponent } from '../../../../../../main/webapp/app/entities/planner/planner.component';
import { PlannerService } from '../../../../../../main/webapp/app/entities/planner/planner.service';
import { Planner } from '../../../../../../main/webapp/app/entities/planner/planner.model';

describe('Component Tests', () => {

    describe('Planner Management Component', () => {
        let comp: PlannerComponent;
        let fixture: ComponentFixture<PlannerComponent>;
        let service: PlannerService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BackOfficeTestModule],
                declarations: [PlannerComponent],
                providers: [
                    PlannerService
                ]
            })
            .overrideTemplate(PlannerComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PlannerComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlannerService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Planner(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.planners[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
